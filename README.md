# Translations

The translations used throughout the [upload.systems](https://upload.systems) site.

## How to Contribute

Fork this repo so you have your own version to work on. Then copy the `en-GB` folder inside `locales`, and replace the language code with the one for your language. A list of language codes can be found [here](https://www.andiamo.co.uk/resources/iso-language-codes/). If your language has a second part to the code, such as the `-GB` in `en-GB`, make sure this part is in capitals.

We will keep the `en-GB` directory up to date with the latest additions, so just use this as the base for what you should translate. Then, you can simply start replacing the strings that you can from your chosen language. Don't worry if there are certain ones which you can't translate, you can just leave these as is.

**Note: We are currently unable to support right-to-left languages on the site. For this reason, please avoid working on translations for these languages, as we won't be able to accept them yet.**

## Things to Note

- Please avoid using Google Translate or translation tools, because these often produce inaccurate results.
- Leave any text inside double curly brackets (`{{example}}`) in English, as these are used to load dynamic content.
- The `meta.json` file contains information about the language you're translating. Please update the `name` field to be the name of the language in that language, and `englishName` to the name of the language in English. `code` should be the same as the name of the directory you are translating. The `flag` field should be the two letter country code. They can be found [here](https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2#Officially_assigned_code_elements). Finally, you can include your upload.systems UUID in the `contributors` list so we can provide you credit on the site.
